import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_live_price.dart';
import 'package:crypto_stats/src/generated/assets.gen.dart';
import 'package:flutter/material.dart';

class CryptoLivePriceCardWidget extends StatelessWidget {
  const CryptoLivePriceCardWidget({
    Key? key,
    required this.cryptoLivePrice,
    required this.cryptoName,
  }) : super(key: key);

  final CryptoLivePrice cryptoLivePrice;
  final String cryptoName;

  Image _getCorrespandantImage() {
    if (cryptoName == 'BTC') {
      return Assets.images.btc.image(
        width: 40,
        height: 40,
      );
    } else if (cryptoName == 'ETH') {
      return Assets.images.eth.image(
        width: 40,
        height: 40,
      );
    } else {
      return Assets.images.ada.image(
        width: 40,
        height: 40,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      width: 250,
      height: 220,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            offset: const Offset(1, 1),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              _getCorrespandantImage(),
              const SizedBox(width: 20),
              RichText(
                text: TextSpan(
                  text: '',
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: cryptoName,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const TextSpan(
                      text: '/USD',
                    ),
                  ],
                ),
              )
            ],
          ),
          const SizedBox(height: 40),
          Text(
            cryptoLivePrice.price.toString(),
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
          const SizedBox(height: 40),
          Text(cryptoLivePrice.takerSide),
        ],
      ),
    );
  }
}
