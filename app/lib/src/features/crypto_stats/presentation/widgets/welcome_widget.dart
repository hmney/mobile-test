import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';

/// documentaiher
class WelcomeWidget extends StatelessWidget {
  ///edghlsdgl
  const WelcomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(l10n.welcome),
        Text(
          l10n.myName,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
