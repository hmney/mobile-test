import 'package:crypto_stats/src/generated/assets.gen.dart';
import 'package:flutter/material.dart';

class ProfileAvatarWidget extends StatelessWidget {
  const ProfileAvatarWidget({
    Key? key,
    required this.width,
    required this.height,
  })  : assert(width > 0),
        assert(height > 0),
        super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage(Assets.images.avatar.path),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
