import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_asset.dart';
import 'package:flutter/material.dart';

class CryptoAssetCardWidget extends StatelessWidget {
  const CryptoAssetCardWidget({Key? key, required this.asset})
      : super(key: key);
  final CryptoAsset asset;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            offset: const Offset(1, 1),
          )
        ],
      ),
      child: Row(
        children: [
          Image.network(
            asset.url,
            width: 30,
            height: 30,
          ),
          const Spacer(),
          Text(
            asset.id,
            style: const TextStyle(color: Colors.black),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
