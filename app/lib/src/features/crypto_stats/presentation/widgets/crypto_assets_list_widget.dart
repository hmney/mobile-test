import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_asset.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/crypto_asset_card_widget.dart';
import 'package:flutter/material.dart';

class CryptoAssetsListWidget extends StatelessWidget {
  CryptoAssetsListWidget({Key? key, required this.cryptoAssets})
      : super(key: key);

  final List<CryptoAsset> cryptoAssets;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemCount: cryptoAssets.length,
        separatorBuilder: (context, index) => const SizedBox(height: 10),
        itemBuilder: (context, index) => CryptoAssetCardWidget(
          asset: cryptoAssets[index],
        ),
      ),
    );
  }
}
