import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:crypto_stats/l10n/l10n.dart';
import 'package:crypto_stats/src/features/crypto_stats/data/models/crypto_live_price_model.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/crypto_live_price_card_widget.dart';
import 'package:flutter/material.dart';

class CryptoLivePricesListWidget extends StatefulWidget {
  const CryptoLivePricesListWidget({Key? key, required this.socket})
      : super(key: key);
  final WebSocket socket;

  @override
  State<CryptoLivePricesListWidget> createState() =>
      _CryptoLivePricesListWidgetState();
}

class _CryptoLivePricesListWidgetState
    extends State<CryptoLivePricesListWidget> {
  final StreamController<CryptoLivePriceModel> _btc =
      StreamController<CryptoLivePriceModel>();

  final StreamController<CryptoLivePriceModel> _eth =
      StreamController<CryptoLivePriceModel>();

  final StreamController<CryptoLivePriceModel> _ada =
      StreamController<CryptoLivePriceModel>();

  @override
  void dispose() {
    _btc.close();
    _eth.close();
    _ada.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return StreamBuilder<dynamic>(
      stream: widget.socket,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final cryptoLivePrice = CryptoLivePriceModel.fromJson(
              json.decode(snapshot.data as String) as Map<String, dynamic>);
          if (cryptoLivePrice.symbolId == 'COINBASE_SPOT_BTC_USD') {
            _btc.add(cryptoLivePrice);
          } else if (cryptoLivePrice.symbolId == 'COINBASE_SPOT_ETH_USD') {
            _eth.add(cryptoLivePrice);
          } else {
            _ada.add(cryptoLivePrice);
          }
          return SizedBox(
            height: 240,
            child: ListView(
              padding: const EdgeInsets.symmetric(vertical: 10),
              scrollDirection: Axis.horizontal,
              children: [
                StreamBuilder<CryptoLivePriceModel>(
                  stream: _btc.stream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData || snapshot.data == null) {
                      return const SizedBox();
                    }
                    return CryptoLivePriceCardWidget(
                      cryptoLivePrice: snapshot.data!,
                      cryptoName: l10n.btc,
                    );
                  },
                ),
                const SizedBox(width: 10),
                StreamBuilder<CryptoLivePriceModel>(
                  stream: _eth.stream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData || snapshot.data == null) {
                      return const SizedBox();
                    }
                    return CryptoLivePriceCardWidget(
                      cryptoLivePrice: snapshot.data!,
                      cryptoName: l10n.eth,
                    );
                  },
                ),
                const SizedBox(width: 10),
                StreamBuilder<CryptoLivePriceModel>(
                  stream: _ada.stream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData || snapshot.data == null) {
                      return const SizedBox();
                    }
                    return CryptoLivePriceCardWidget(
                      cryptoLivePrice: snapshot.data!,
                      cryptoName: l10n.ada,
                    );
                  },
                ),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
