import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';

class BalanceCardWidget extends StatelessWidget {
  const BalanceCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: const Color(0xFF516EFB),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            l10n.balanceTitle,
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
          Text(
            l10n.balance,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 28,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    l10n.monthlyProfitTitle,
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    l10n.monthlyProfit,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              const _PercentChipWdiget(),
            ],
          )
        ],
      ),
    );
  }
}

class _PercentChipWdiget extends StatelessWidget {
  const _PercentChipWdiget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Chip(
      avatar: const Icon(
        Icons.arrow_drop_up_rounded,
        color: Colors.green,
      ),
      label: Text(
        l10n.tenPerCent,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: const Color(0xFF516EFB).withAlpha(200),
      padding: EdgeInsets.zero,
    );
  }
}
