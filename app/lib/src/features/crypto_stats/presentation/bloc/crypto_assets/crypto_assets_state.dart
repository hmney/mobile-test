part of 'crypto_assets_bloc.dart';

abstract class CryptoAssetsState extends Equatable {
  const CryptoAssetsState();
  @override
  List<Object> get props => [];
}

class CryptoAssetsLoadInProgress extends CryptoAssetsState {}

class CryptoAssetsLoadSuccess extends CryptoAssetsState {
  CryptoAssetsLoadSuccess({required this.cryptoAssets});

  final List<CryptoAsset> cryptoAssets;

  @override
  List<Object> get props => [cryptoAssets];

  @override
  String toString() =>
      'CryptoAssetsLoadSuccess { cryptoAssets: $cryptoAssets }';
}

class CryptoAssetsLoadFailure extends CryptoAssetsState {}
