part of 'crypto_assets_bloc.dart';

abstract class CryptoAssetsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CryptoAssetsLoaded extends CryptoAssetsEvent {}
