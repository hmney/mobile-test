import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_asset.dart';
import 'package:crypto_stats/src/features/crypto_stats/domain/repositories/crypto_stats_repositroy.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'crypto_assets_event.dart';
part 'crypto_assets_state.dart';

class CryptoAssetsBloc extends Bloc<CryptoAssetsEvent, CryptoAssetsState> {
  final CryptoStatsRepositroy cryptoStatsRepositroy;
  CryptoAssetsBloc({required this.cryptoStatsRepositroy})
      : super(CryptoAssetsLoadInProgress());

  @override
  Stream<CryptoAssetsState> mapEventToState(CryptoAssetsEvent event) async* {
    if (event is CryptoAssetsLoaded) {
      yield* _mapCryptoAssetsLoadedToState();
    }
  }

  Stream<CryptoAssetsState> _mapCryptoAssetsLoadedToState() async* {
    try {
      final cryptoAssets = await cryptoStatsRepositroy.loadCryptoAssets();
      if (cryptoAssets != null) {
        yield CryptoAssetsLoadSuccess(cryptoAssets: cryptoAssets);
      } else {
        yield CryptoAssetsLoadFailure();
      }
    } catch (_) {
      yield CryptoAssetsLoadFailure();
    }
  }
}
