part of 'crypto_live_prices_bloc.dart';

abstract class CryptoLivePricesEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CryptoLivePricesLoaded extends CryptoLivePricesEvent {}
