part of 'crypto_live_prices_bloc.dart';

abstract class CryptoLivePricesState extends Equatable {
  const CryptoLivePricesState();
  @override
  List<Object> get props => [];
}

class CryptoLivePricesLoadInProgress extends CryptoLivePricesState {}

class CryptoLivePricesLoadSuccess extends CryptoLivePricesState {
  const CryptoLivePricesLoadSuccess({required this.cryptoLivePricesSocket});

  final WebSocket cryptoLivePricesSocket;

  @override
  List<Object> get props => [cryptoLivePricesSocket];

  @override
  String toString() =>
      'CryptoLivePricesLoadSuccess { cryptoLivePrices: $cryptoLivePricesSocket }';
}

class CryptoLivePricesLoadFailure extends CryptoLivePricesState {}
