import 'dart:io';

import 'package:crypto_stats/src/features/crypto_stats/domain/repositories/crypto_stats_repositroy.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'crypto_live_prices_event.dart';
part 'crypto_live_prices_state.dart';

class CryptoLivePricesBloc
    extends Bloc<CryptoLivePricesEvent, CryptoLivePricesState> {
  CryptoLivePricesBloc({required this.cryptoStatsRepositroy})
      : super(CryptoLivePricesLoadInProgress());
  final CryptoStatsRepositroy cryptoStatsRepositroy;

  @override
  Stream<CryptoLivePricesState> mapEventToState(
      CryptoLivePricesEvent event) async* {
    if (event is CryptoLivePricesLoaded) {
      yield* _mapCryptoLivePricesLoadedToState();
    }
  }

  Stream<CryptoLivePricesState> _mapCryptoLivePricesLoadedToState() async* {
    try {
      final socket = await cryptoStatsRepositroy.loadWebSocket();
      yield CryptoLivePricesLoadSuccess(cryptoLivePricesSocket: socket);
    } catch (_) {
      yield CryptoLivePricesLoadFailure();
    }
  }
}
