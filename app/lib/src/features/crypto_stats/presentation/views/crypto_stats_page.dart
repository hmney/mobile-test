import 'package:crypto_stats/l10n/l10n.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/bloc/crypto_assets/crypto_assets_bloc.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/bloc/crypto_live_prices/crypto_live_prices_bloc.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/balance_card_widget.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/crypto_assets_list_widget.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/crypto_live_prices_list_widget.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/profile_avatar_widget.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/widgets/welcome_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// {@template crypto_stats_page}
///  Page that hanldes the user interface for crypto stats feature.
/// {@endtemplate}
class CryptoStatsPage extends StatefulWidget {
  /// {@macro crypto_stats_page}
  const CryptoStatsPage({Key? key}) : super(key: key);

  @override
  State<CryptoStatsPage> createState() => _CryptoStatsPageState();
}

class _CryptoStatsPageState extends State<CryptoStatsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffFAFAFA),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: const [
                _TopSectionWidget(),
                SizedBox(height: 40),
                _BalanceSectionWidget(),
                SizedBox(height: 20),
                _LivePricesSectionWidget(),
                SizedBox(height: 20),
                _CryptoAssetsSectionWidget()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _TopSectionWidget extends StatelessWidget {
  const _TopSectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          WelcomeWidget(),
          ProfileAvatarWidget(
            width: 40,
            height: 40,
          )
        ],
      ),
    );
  }
}

class _BalanceSectionWidget extends StatelessWidget {
  const _BalanceSectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: BalanceCardWidget(),
    );
  }
}

class _LivePricesSectionWidget extends StatelessWidget {
  const _LivePricesSectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            l10n.livePrices,
          ),
          const SizedBox(height: 10),
          BlocBuilder<CryptoLivePricesBloc, CryptoLivePricesState>(
            builder: (context, state) {
              if (state is CryptoLivePricesLoadSuccess) {
                return CryptoLivePricesListWidget(
                    socket: state.cryptoLivePricesSocket);
              }
              return const SizedBox();
            },
          ),
        ],
      ),
    );
  }
}

class _CryptoAssetsSectionWidget extends StatelessWidget {
  const _CryptoAssetsSectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(l10n.cryptoAssets),
        ),
        const SizedBox(height: 20),
        BlocBuilder<CryptoAssetsBloc, CryptoAssetsState>(
          builder: (context, state) {
            if (state is CryptoAssetsLoadSuccess) {
              return CryptoAssetsListWidget(cryptoAssets: state.cryptoAssets);
            }
            return const SizedBox();
          },
        ),
      ],
    );
  }
}
