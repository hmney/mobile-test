import 'dart:io';

import 'package:crypto_stats/src/features/crypto_stats/data/models/crypto_asset_model.dart';

abstract class CryptoStatsRepositroy {
  Future<List<CryptoAssetModel>?> loadCryptoAssets();
  Future<WebSocket> loadWebSocket();
}
