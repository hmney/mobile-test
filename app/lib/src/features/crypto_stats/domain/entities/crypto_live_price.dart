import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

class CryptoLivePrice extends Equatable {
  const CryptoLivePrice(
      {required this.price, required this.takerSide, required this.symbolId});

  final double price;
  @JsonKey(name: 'taker_side')
  final String takerSide;

  @JsonKey(name: 'symbol_id')
  final String symbolId;

  @override
  List<Object?> get props => [price, takerSide, symbolId];
}
