import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

class CryptoAsset extends Equatable {
  const CryptoAsset({required this.id, required this.url});

  @JsonKey(name: 'asset_id', defaultValue: '')
  final String id;
  @JsonKey(defaultValue: '')
  final String url;

  @override
  List<Object?> get props => [id, url];
}
