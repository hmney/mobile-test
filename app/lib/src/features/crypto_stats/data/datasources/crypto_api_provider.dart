import 'dart:convert';
import 'dart:io';

import 'package:crypto_stats/src/features/crypto_stats/data/models/crypto_asset_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class CryptoApiProvider {
  final _dioClient = Dio();
  final String? _yourApiKey = dotenv.env['COIN_API_KEY'];
  Future<WebSocket> connectWebSocket() async {
    try {
      final baseURI = Uri.parse('wss://ws-sandbox.coinapi.io/v1/');

      final _uri = Uri(
        scheme: baseURI.scheme,
        host: baseURI.host,
        port: baseURI.port,
        path: baseURI.path,
      );

      final socket = await WebSocket.connect(_uri.toString());

      socket.add(
        json.encode({
          'type': 'hello',
          'apikey': _yourApiKey,
          'heartbeat': false,
          'subscribe_data_type': ['trade'],
          'subscribe_filter_symbol_id': [
            r'COINBASE_SPOT_BTC_USD$',
            r'COINBASE_SPOT_ETH_USD$',
            r'COINBASE_SPOT_ADA_USD$',
          ]
        }),
      );
      return socket;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<CryptoAssetModel>> getCryptoAssets() async {
    try {
      final result = await _dioClient.get<List<dynamic>>(
        'https://rest.coinapi.io/v1/assets/icons/100',
        options: Options(
          headers: <String, String>{'X-CoinAPI-Key': _yourApiKey!},
        ),
      );
      return result.data!
          .map((dynamic e) =>
              CryptoAssetModel.fromJson(e as Map<String, dynamic>))
          .toList();
    } catch (e) {
      rethrow;
    }
  }
}
