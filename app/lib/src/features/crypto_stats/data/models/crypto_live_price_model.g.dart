// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crypto_live_price_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CryptoLivePriceModel _$CryptoLivePriceModelFromJson(
        Map<String, dynamic> json) =>
    CryptoLivePriceModel(
      price: (json['price'] as num).toDouble(),
      takerSide: json['taker_side'] as String,
      symbolId: json['symbol_id'] as String,
    );

Map<String, dynamic> _$CryptoLivePriceModelToJson(
        CryptoLivePriceModel instance) =>
    <String, dynamic>{
      'price': instance.price,
      'taker_side': instance.takerSide,
      'symbol_id': instance.symbolId,
    };
