// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crypto_asset_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CryptoAssetModel _$CryptoAssetModelFromJson(Map<String, dynamic> json) =>
    CryptoAssetModel(
      id: json['asset_id'] as String? ?? '',
      url: json['url'] as String? ?? '',
    );

Map<String, dynamic> _$CryptoAssetModelToJson(CryptoAssetModel instance) =>
    <String, dynamic>{
      'asset_id': instance.id,
      'url': instance.url,
    };
