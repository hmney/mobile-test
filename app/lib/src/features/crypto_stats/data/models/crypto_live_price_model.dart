// ignore_for_file: public_member_api_docs

import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_live_price.dart';
import 'package:json_annotation/json_annotation.dart';

part 'crypto_live_price_model.g.dart';

@JsonSerializable()
class CryptoLivePriceModel extends CryptoLivePrice {
  const CryptoLivePriceModel({
    required double price,
    required String takerSide,
    required String symbolId,
  }) : super(price: price, takerSide: takerSide, symbolId: symbolId);

  factory CryptoLivePriceModel.fromJson(Map<String, dynamic> json) =>
      _$CryptoLivePriceModelFromJson(json);

  Map<String, dynamic> toJson() => _$CryptoLivePriceModelToJson(this);
}
