// ignore_for_file: public_member_api_docs

import 'package:crypto_stats/src/features/crypto_stats/domain/entities/crypto_asset.dart';
import 'package:json_annotation/json_annotation.dart';

part 'crypto_asset_model.g.dart';

@JsonSerializable()
class CryptoAssetModel extends CryptoAsset {
  const CryptoAssetModel({
    required String id,
    required String url,
  }) : super(id: id, url: url);

  factory CryptoAssetModel.fromJson(Map<String, dynamic> json) =>
      _$CryptoAssetModelFromJson(json);

  Map<String, dynamic> toJson() => _$CryptoAssetModelToJson(this);
}
