import 'dart:io';

import 'package:crypto_stats/src/features/crypto_stats/data/datasources/crypto_api_provider.dart';
import 'package:crypto_stats/src/features/crypto_stats/data/models/crypto_asset_model.dart';
import 'package:crypto_stats/src/features/crypto_stats/domain/repositories/crypto_stats_repositroy.dart';

class CryptoStatsRepositoryImplementation extends CryptoStatsRepositroy {
  CryptoStatsRepositoryImplementation();

  final CryptoApiProvider _cryptoApiProvider = CryptoApiProvider();

  @override
  Future<WebSocket> loadWebSocket() async {
    try {
      return _cryptoApiProvider.connectWebSocket();
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<CryptoAssetModel>> loadCryptoAssets() async {
    try {
      return await _cryptoApiProvider.getCryptoAssets();
    } catch (e) {
      rethrow;
    }
  }
}
