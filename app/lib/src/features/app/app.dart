import 'package:crypto_stats/src/features/crypto_stats/crypto_stats.dart';
import 'package:crypto_stats/src/features/crypto_stats/data/repositories/crypto_stats_repository_implementation.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/bloc/crypto_assets/crypto_assets_bloc.dart';
import 'package:crypto_stats/src/features/crypto_stats/presentation/bloc/crypto_live_prices/crypto_live_prices_bloc.dart';
import 'package:crypto_stats/src/generated/fonts.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

/// {@template app}
/// The widget that handles the dependency injection of your application.
/// {@endtemplate}
class App extends StatelessWidget {
  /// {@macro app}
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const AppView();
  }
}

/// {@template app_view}
/// The widget that configures your application.
/// {@endtemplate}
class AppView extends StatelessWidget {
  /// {@macro app_view}
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      restorationScopeId: 'app',
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''), // English, no country code.
        Locale('es', ''), // Span
      ],
      theme: ThemeData(fontFamily: FontFamily.poppins),
      darkTheme: ThemeData.dark(),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) {
              return CryptoAssetsBloc(
                cryptoStatsRepositroy: CryptoStatsRepositoryImplementation(),
              )..add(CryptoAssetsLoaded());
            },
          ),
          BlocProvider(
            create: (context) {
              return CryptoLivePricesBloc(
                cryptoStatsRepositroy: CryptoStatsRepositoryImplementation(),
              )..add(CryptoLivePricesLoaded());
            },
          ),
        ],
        child: const CryptoStatsPage(),
      ),
    );
  }
}
