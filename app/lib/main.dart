import 'package:crypto_stats/src/features/app/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
  // Run the app and pass its dependencies to it.
  await dotenv.load();

  runApp(const App());
}
